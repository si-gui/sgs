#!/bin/sh
if [ $# != 1 ] ;then
	echo "You should use it like:"
	echo "bash get-obsworker-status.sh [all/obs-workername]"
	exit 1
fi
rm -rf all-worker.txt all-workers.lst res.txt

#get all worker's status
curl -s --user Sunguoshuai:421884 -X GET https://build.openeuler.org/worker/_status -o all-worker.txt

cat all-worker.txt | grep "workerid=\"*\"" | sort -u |awk '{for(i=2;i<=NF;++i) printf $i "\t";printf "\n"}' | awk '{print $1}'| sort -u | awk -F = '{print $2}' | sed 's/"//g' | awk -F : '{print $1}' | sort -u > all-workers.lst


if [ $1 == all ];then

for workers in `cat all-workers.lst`;do
WORKID=$workers
#get all_instance_num and building_instance_num
all_instance_num=`cat all-worker.txt | grep "$WORKID:" | grep  -E "<building|<idle|<down" | awk '{print $2}' | awk -F= '{print $2}' | sed 's/"//g' | awk -F: '{print $2}' | sort -u | tail -1`
building_instance_num=`cat all-worker.txt | grep "$WORKID:" | grep  -E "<building" | awk '{print $2}' | awk -F= '{print $2}' |sort -u | wc -l`
echo "$WORKID $all_instance_num $building_instance_num" >> res.txt
done

else
WORKID=$1
#get all_instance_num and building_instance_num
all_instance_num=`cat all-worker.txt | grep "$WORKID:" | grep  -E "<building|<idle|<down" | awk '{print $2}' | awk -F= '{print $2}' | sed 's/"//g' | awk -F: '{print $2}' | sort -u | tail -1`
building_instance_num=`cat all-worker.txt | grep "$WORKID:" | grep  -E "<building" | awk '{print $2}' | awk -F= '{print $2}' |sort -u | wc -l`
echo "$WORKID $all_instance_num $building_instance_num" >> res.txt
fi

cat res.txt
rm -rf all-worker.txt all-workers.lst res.txt
