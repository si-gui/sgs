#!/bin/sh
set -x
if [ $# != 2 ];then
	echo "USAGE ERROR,YOU SHOULD RUN AS:"
	echo "bash get-all-values-by-IP.sh 172.16.1.45:9100 [all/load/CPU/mem/disk/IOPS/network]"
	exit 1
fi
user="prometheus"
#pass="szpzc@$#@!"
echo "szpzc@\$#@!" > ~/.prome_secrect
Instance=$1
echo $Instance

if [ $2 == all ];then
# node load 5min 
curl -g --user prometheus:$(cat ~/.prome_secrect)  "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=node_load5{instance=\"$Instance\"}" | json
# cpu usage_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=avg(irate(node_cpu_seconds_total{instance=\"$Instance\",mode=\"idle\"}[5m]))by(instance)*100" | json
# mem usage_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=100%20-%20((node_memory_MemFree_bytes{instance=\"$Instance\"}%2Bnode_memory_Cached_bytes{instance=\"$Instance\"}%2Bnode_memory_Buffers_bytes{instance=\"$Instance\"})%2Fnode_memory_MemTotal_bytes{instance=\"$Instance\"})*100" | json
#disk uasge_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=100%20-%20node_filesystem_free_bytes{instance=\"$Instance\",fstype!~\"rootfs|selinuxfs|autofs|rpc_pipefs|tmpfs|udev|none|devpts|sysfs|debugfs|fuse.*\"}/node_filesystem_size_bytes{instance=\"$Instance\",fstype!~\"rootfs|selinuxfs|autofs|rpc_pipefs|tmpfs|udev|none|devpts|sysfs|debugfs|fuse.*\"}*100" | json
#disk IOPS
#read
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_reads_completed_total{instance=\"$Instance\",device=~\"[a-z]*[a-z]\"}[5m])" | json
#write
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_writes_completed_total{instance=\"$Instance\",device=~\"[a-z]*[a-z]\"}[5m])" | json
#weighted-IO duilieshu
#curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_weighted_seconds_total{instance=\"$Instance\"}[5m])"
#network
#recive
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=sum(irate(node_network_receive_bytes_total{instance=\"$Instance\",device!~\"bond.*?|lo\"}[5m])/128)by(instance)" | json
#transmit
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=sum(irate(node_network_transmit_bytes_total{instance=\"$Instance\",device!~\"bond.*?|lo\"}[5m])/128)by(instance)" | json

elif [ $2 == load ];then
# node load 5min 
curl -g --user prometheus:$(cat ~/.prome_secrect)  "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=node_load5{instance=\"$Instance\"}" | json
elif [ $2 == CPU ];then
# cpu usage_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=avg(irate(node_cpu_seconds_total{instance=\"$Instance\",mode=\"idle\"}[5m]))by(instance)*100" | json

elif [ $2 == mem ];then
# mem usage_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=100%20-%20((node_memory_MemFree_bytes{instance=\"$Instance\"}%2Bnode_memory_Cached_bytes{instance=\"$Instance\"}%2Bnode_memory_Buffers_bytes{instance=\"$Instance\"})%2Fnode_memory_MemTotal_bytes{instance=\"$Instance\"})*100" | json

elif [ $2 == disk ];then
#disk uasge_rate
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=100%20-%20node_filesystem_free_bytes{instance=\"$Instance\",fstype!~\"rootfs|selinuxfs|autofs|rpc_pipefs|tmpfs|udev|none|devpts|sysfs|debugfs|fuse.*\"}/node_filesystem_size_bytes{instance=\"$Instance\",fstype!~\"rootfs|selinuxfs|autofs|rpc_pipefs|tmpfs|udev|none|devpts|sysfs|debugfs|fuse.*\"}*100" | json

elif [ $2 == IOPS ];then
#disk IOPS
#read
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_reads_completed_total{instance=\"$Instance\",device=~\"[a-z]*[a-z]\"}[5m])" | json
#write
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_writes_completed_total{instance=\"$Instance\",device=~\"[a-z]*[a-z]\"}[5m])" | json
#weighted-IO duilieshu
#curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=rate(node_disk_weighted_seconds_total{instance=\"$Instance\"}[5m])"

elif [ $2 == network ];then
#network
#recive
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=sum(irate(node_network_receive_bytes_total{instance=\"$Instance\",device!~\"bond.*?|lo\"}[5m])/128)by(instance)" | json
#transmit
curl -g --user prometheus:$(cat ~/.prome_secrect)   "https://openeuler-beijing4-prometheus.osinfra.cn/api/v1/query?query=sum(irate(node_network_transmit_bytes_total{instance=\"$Instance\",device!~\"bond.*?|lo\"}[5m])/128)by(instance)" | json
fi








